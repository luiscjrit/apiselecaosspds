package com.apiselecao.ApiSelecao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSelecaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiSelecaoApplication.class, args);
	}

}