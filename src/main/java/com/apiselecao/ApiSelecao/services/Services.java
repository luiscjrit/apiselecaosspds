package com.apiselecao.ApiSelecao.services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Optional;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apiselecao.ApiSelecao.entidades.User;
import com.apiselecao.ApiSelecao.repository.UserRepository;

@Service
public class Services {

	private static String LOGIN_ADMIN = "admin";
	private static String SENHA_ADMIN = "E10ADC3949BA59ABBE56E057F20F883E";
	
	@Autowired
	private UserRepository userRepository;
	
	public User buscar(Long id) {
		Optional<User> usuario = userRepository.findById(id);
		return usuario.get();
	}
	
	public User salvar(User usuario) {
		usuario.setSenha(gerarMd5(usuario.getSenha()));
		if(!autenticacao(usuario.getLogin(),usuario.getSenha())) {
			usuario.setMensagem("Não Autorizado. Necessário inserir login e senha corretos");
			return usuario;
		}else {
			usuario.setDateCreate(new Date());
			User novoUsuario = userRepository.save(usuario); 
			novoUsuario.setMensagem("Usuário " + novoUsuario.getName() + " inserido com sucesso. ID: " + novoUsuario.getId());
			return novoUsuario;
		}
	}
	
	public String deletar(Long id, String login, String senha) {
		if(!autenticacao(login,gerarMd5(senha))) {
			return "Não Autorizado. Necessário inserir login e senha corretos";
		}else {
			User usuarioParaDeletar = buscar(id);
			userRepository.delete(usuarioParaDeletar);
			return "Usuário deletado com sucesso";
		}
	}
	
	private Boolean autenticacao(String login, String senha) {
		if(!login.equals(LOGIN_ADMIN) || !senha.equals(SENHA_ADMIN)) {
			return false;
		}
		return true;
	}
	
	private String gerarMd5(String senha) {
	    MessageDigest md5;
		try {
			md5 = MessageDigest.getInstance("MD5");
			md5.update(senha.getBytes());
		    byte[] digest = md5.digest();
		    String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
		    return myHash;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
		
	}
}
