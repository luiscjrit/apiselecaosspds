package com.apiselecao.ApiSelecao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.apiselecao.ApiSelecao.entidades.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> { }