package com.apiselecao.ApiSelecao.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.apiselecao.ApiSelecao.dto.UserDto;
import com.apiselecao.ApiSelecao.entidades.User;
import com.apiselecao.ApiSelecao.services.Services;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private Services userServices;
	
	@PostMapping(path = "/post")
	@ResponseBody
	public String Post(@RequestBody @Valid UserDto usuarioDto) {
		User usuario = userServices.salvar(usuarioDto.transformaParaObjeto());
		return usuario.getMensagem();
	}
	
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<User> GetById(@PathVariable("id") Long id) {
		User usuario = userServices.buscar(id);
		if(usuario == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else
        	return new ResponseEntity<User>(usuario, HttpStatus.OK);
	}
	
	@DeleteMapping(path = "/delete/{id}/{login}/{senha}")
	@ResponseBody
    public String Delete(@PathVariable("id") Long id, 
    		@PathVariable("login") String login,@PathVariable("senha") String senha)
    {
        return userServices.deletar(id, login, senha);
    }
	
}
