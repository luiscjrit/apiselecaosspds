package com.apiselecao.ApiSelecao.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.apiselecao.ApiSelecao.entidades.User;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;

@Getter
public class UserDto implements Serializable {

	private static final long serialVersionUID = 1L;

	 	private Long id;

	 	@NotBlank(message = "{name.not.blank}")
	    private String name;
	       
	    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	    private Date dateCreate;
	    
	    @NotBlank(message = "{login.not.blank}")
	    private String login;
	    
	    @NotBlank(message = "{senha.not.blank}")	    
	    private String senha;
	    
	    public User transformaParaObjeto(){
	        return new User(name,dateCreate,login,senha);
	    }

		public UserDto() {
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Date getDateCreate() {
			return dateCreate;
		}

		public void setDateCreate(Date dateCreate) {
			this.dateCreate = dateCreate;
		}

		public String getLogin() {
			return login;
		}

		public void setLogin(String login) {
			this.login = login;
		}

		public String getSenha() {
			return senha;
		}

		public void setSenha(String senha) {
			this.senha = senha;
		}
}
